package com.example.notifffier

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class NotifffierApplication

fun main(args: Array<String>) {
	runApplication<NotifffierApplication>(*args)
}
